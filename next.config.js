// whitelist domain, for img component
// needed to ensure that external urls can't be abused
module.exports = {
  images: {
    domains: ['images.ctfassets.net'],
  },
};


// Vercel webhook - Settings - Git - Deploy hooks
// Contentful hook - Settings - Webhooks - Vercel Deploy a site (Add)
