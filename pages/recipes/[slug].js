import { createClient } from 'contentful';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import Image from 'next/image';
import Skeleton from '../../components/Skeleton';

const client = createClient({
  space: process.env.CONTENTFUL_SPACE_ID,
  accessToken: process.env.CONTENTFUL_ACCESS_KEY,
});

// создаёт @ build-time пре-рендеренные страницы с указанными путями из paths
// до запуска, next.js не знает заранее какие пути будут, через данную функцию он получает объект со всеми путями
// отдельно, для каждого пути, передает аргумент context в getStaticProps, и в самом уже компоненте получаем ОТДЕЛЬНО инф. по данному пути
export const getStaticPaths = async () => {
  const res = await client.getEntries({
    content_type: 'recipe',
  });

  const paths = res.items.map((item) => {
    return {
      params: {
        slug: item.fields.slug,
      },
    };
  });

  return {
    paths,
    fallback: true,
  };
};

export async function getStaticProps({ params }) {
  // to limit what we can get from Contentful
  const { items } = await client.getEntries({
    content_type: 'recipe',
    'fields.slug': params.slug,
  });

  // if data not found, redirect
  if (!items.length) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  // revalidate - An optional amount in seconds after which a page re-generation can occur.
  // example: if set to 10sec - at first visit, Next.js is going to query data for updates, however if someone again comes to this page within 10 seconds - it won't query again. Only after a 10sec wait, will it try again

  return {
    props: {
      recipe: items[0],
    },
    revalidate: 1,
  };
}

export default function RecipeDetails({ recipe }) {
  // in the bg, Next.js is rerunning getStaticProps to fetch a new data, inject new props into a component
  if (!recipe) return <Skeleton />;

  const {
    featuredImage,
    title,
    cookingTime,
    ingredients,
    method,
  } = recipe.fields;
  return (
    <div>
      <div className="banner">
        <Image
          src={'https:' + featuredImage.fields.file.url}
          width={featuredImage.fields.file.details.image.width}
          height={featuredImage.fields.file.details.image.height}
        />
        <h2>{title}</h2>
      </div>
      <div className="info">
        <p>Takes about {cookingTime} mins to cook.</p>
        <h3>Ingredients:</h3>
        {ingredients.map((ingredient) => (
          <span key={ingredient}>{ingredient}</span>
        ))}
      </div>
      <div className="method">
        <h3>Method:</h3>
        <div className=""> {documentToReactComponents(method)}</div>
      </div>
      <style jsx>{`
      h2,h3 {
          text-transform: uppercase;
        }
        .banner h2 {
          margin: 0;
          background: #fff;
          display: inline-block;
          padding: 20px;
          position: relative;
          top: -60px;
          left: -10px;
          transform: rotateZ(-1deg);
          box-shadow: 1px 3px 5px rgba(0,0,0,0.1);
        }
        .info p {
          margin: 0;
        }
        .info span::after {
          content: ", ";
        }
        .info span:last-child::after {
          content: ".";
          `}</style>
    </div>
  );
}
